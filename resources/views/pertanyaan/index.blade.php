@extends('layout.master')
@section('title', 'List Pertanyaan')
@section('content')
    <a href="/pertanyaan/create" class="btn btn-primary mb-2"> Tambah </a>
    <table class="table">
        <thead class="thead-light">
            <tr>
                <th scope="col">#</th>
                <th scope="col">Judul</th>
                <th scope="col">Isi</th>
                <th scope="col">Aksi</th>
            </tr>
        </thead>

        <tbody>
            {{-- <td>1</td>
            <td>test judul </td>
            <td>test isi</td> --}}
            @forelse ($pertanyaan as $key=>$value)
                <tr> 
                    <td>{{$key + 1}}</td>
                    <td>{{$value->judul}}</td>
                    <td>{{$value->isi}}</td>
                    <td>
                        <form action="/pertanyaan/{{$value->id}}" method="POST">
                            <a href="/pertanyaan/{{$value->id}}" class="btn btn-info"> Show </a>
                            <a href="/pertanyaan/{{$value->id}}/edit" class="btn btn-primary"> Edit </a>
                            @csrf
                            @method('DELETE')
                            <input type="submit" class="btn btn-danger my-1" value="Delete">
                        </form>
                    </td>
                </tr>
            @empty
                <tr colspan="3">
                    <td> No Data </td>
                </tr>
            @endforelse
        </tbody>
    </table>
@endsection
    
